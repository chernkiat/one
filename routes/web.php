<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

Route::resource('documents', 'DocumentController')
    ->except('show');
Route::get('documents/{document}/crop', 'DocumentController@crop')->name('documents.crop');
Route::get('documents/{document}/export', 'DocumentController@export')->name('documents.export');

Route::prefix('system')->namespace('Admin')->group(function() {
    Route::get('documents/{document}/crop', 'RouteTestController@test')->name('test');
});
