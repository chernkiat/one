const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/app.js', 'public/js')
    .js([
    'resources/js/vendor.js',
    'public/js/delete.handler.js',
    ], 'public/js/vendor.js')

    // CropperJS
    .js('resources/js/crop.js', 'public/js')
    .combine([
        'node_modules/cropperjs/dist/cropper.css',
    ], 'public/css/crop.css')

    .sass('resources/sass/fontawesome.scss', 'public/css')
    .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');
    // .less('node_modules/@fortawesome/fontawesome-free/less/fontawesome.less', 'public/css')
    // .copy('node_modules/@fortawesome/fontawesome-free/css/all.min.css', 'public/css/myFontawesome.css');
