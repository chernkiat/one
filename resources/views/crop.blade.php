@extends('layouts.app')

@section ('styles')
<link href="/css/crop.css" rel="stylesheet">
<link id="myDynamicLink" href="#" rel="stylesheet">
<style>
    .pagination {
        align-items: center;
        justify-content: center;
        margin-bottom: 0px;
    }

    button:disabled {
        cursor: not-allowed;
    }

    img {
        display: block;
        max-width: 100%; /* a must */
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $data->appends($_GET)->links() }}</div>

                <div class="card-body">
                    <?php
                        $page = $data->first();
                        $document = $page->document;
                        $pathFront = "/storage/{$document->folder}/{$document->filename}/{$document->filename}-{$data->currentPage()}";
                    ?>
                    <div class="row">
                        <div class="col-md-9">
                            <img src='{{ "{$pathFront}-1.jpg" }}' width="100%" alt="default image" />
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-primary btn-block myProcessButton" data-link="{{ "{$pathFront}-1.jpg" }}" v-on:click="process_button_clicked">
                                <i class="fas fa-tools"></i> {{ trans('default.process') }}
                            </button>
                            <button type="button" class="btn btn-success btn-block myExportButton" data-counter="1" data-link="{{ "{$pathFront}-1.jpg" }}" data-toggle="modal" data-target="#myExportModal">
                                <i class="fas fa-print"></i> {{ trans('default.export') }}
                            </button>
                            <button type="button" id="myExportAllButton" class="btn btn-danger btn-block" data-counter="1" data-link="{{ "{$pathFront}-1.jpg" }}" data-toggle="modal" data-target="#myPreviewModal">
                                <i class="fas fa-download"></i> {{ trans('default.export_all') }}
                            </button>
                        </div>
                    </div>

                    <hr/>

                    <div class="row">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-primary btn-block myProcessButton">
                                <i class="fas fa-tools"></i> {{ trans('default.process') }}
                            </button>
                            <button type="button" class="btn btn-success btn-block myExportButton">
                                <i class="fas fa-print"></i> {{ trans('default.export') }}
                            </button>
                            <button type="button" id="myExportAllButton" class="btn btn-danger btn-block">
                                <i class="fas fa-trash-alt"></i> {{ trans('default.delete') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myProcessModal" ref="process_modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('default.process') }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-9" style="padding-left: 30px;">
                                <div>
                                    <img id="myProcessImage" :src="process_modal_img_src" width="100%" alt="process image" />
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-right: 30px;">
                                <button type="button" id="cropButton" class="btn btn-primary btn-block" :disabled="process_modal_crop_is_disabled" v-on:click="crop_button_clicked">{{ trans('default.crop') }}</button>
                                <button type="button" id="maskButton" class="btn btn-secondary btn-block" :disabled="process_modal_mask_is_disabled" v-on:click="mask_button_clicked">{{ trans('default.mask') }}</button>
                                <button type="button" id="confirmButton" class="btn btn-success btn-block" :disabled="process_modal_confirm_is_disabled" v-on:click="confirm_button_clicked">{{ trans('default.confirm') }}</button>
                                <button type="button" id="cancelButton" class="btn btn-danger btn-block" :disabled="process_modal_cancel_is_disabled" v-on:click="cancel_button_clicked">{{ trans('default.cancel') }}</button>
                                <div id="myProcessPreview" class="divPreviewCropperJS" style="margin-top:8px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-9" style="padding-left: 30px;">
                                <div>
                                    <img id="myFinalImage" :src="process_modal_final_src" width="100%" alt="final image" />
                                </div>
                            </div>
                            <div class="col-md-3 d-flex flex-column" style="padding-right: 30px;">
                                <button type="button" id="saveButton" class="btn btn-primary btn-block" v-on:click="save_button_clicked">{{ trans('default.save') }}</button>
                                <button type="button" class="btn btn-default btn-block mt-auto" data-dismiss="modal">{{ trans("default.close") }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myExportModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('default.export') }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" action="">
                            <input type="hidden" name="_token" value="">
                            <div class="form-group">
                                <label class="control-label">E-Mail Address</label>
                                <div>
                                    <input type="email" class="form-control input-lg" name="email" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-success">Login</button>

                                    <a class="btn btn-link" href="">Forgot Your Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans("admin.ok") }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="/js/crop.js"></script>
<script>
    $(document).ready(function () {
        var cropper;
        var croppable = false;
        var vueObjectName = new Vue({
            el: '#app',
            data: {
                process_modal_task: "",
                process_modal_img_src: "",
                process_modal_final_src: "",
                process_modal_crop_is_disabled: false,
                process_modal_mask_is_disabled: false,
                process_modal_confirm_is_disabled: true,
                process_modal_cancel_is_disabled: true,

                export_modal_task: ""
            },
            methods: {
                process_button_clicked: function(event) {
                    this.process_modal_img_src = event.target.getAttribute("data-link");
                    this.process_modal_final_src = event.target.getAttribute("data-link");
                    this.toggle_process_modal_buttons_to_default(true);
                    $('#myProcessModal').modal('show');
                },
                crop_button_clicked: function(event) {
                    this.process_modal_task = "crop";
                    this.toggle_process_modal_buttons_to_default(false);
                    var image = document.getElementById('myProcessImage');
                    // var preview = document.getElementById('myProcessPreview');
                    cropper = new myCropperJS(image, {
                        viewMode: 1,
                        guides: true, // show tic tac toe lines on cropper
                        center: true, // show fps + mark on cropper
                        highlight: true,
                        cropBoxMovable: true, // if true, when cursor within cropper cannot create new cropper by dragging, can move the existing cropper only
                        cropBoxResizable: true, // if false, after create cropper by dragging cannot resize
                        ready: function () {
                            croppable = true;
                            changeCSS("/css/myCropperJS/previewCropperResult.css");
                            // var clone = this.cloneNode();

                            // clone.removeAttribute("class");
                            // clone.removeAttribute("id");
                            // clone.style.cssText = (
                            //     'display: block;' +
                            //     'width: 100%;' +
                            //     'min-width: 0;' +
                            //     'min-height: 0;' +
                            //     'max-width: none;' +
                            //     'max-height: none;'
                            // );

                            // preview.appendChild(clone.cloneNode());
                        }// ,

                        // crop: function (event) { /* fire before the ready event 1 / 2 times & everytime you move the crop box */
                        //     if (!croppable) {
                        //         return;
                        //     }

                        //     var data = event.detail;
                        //     var imageData = this.cropper.getImageData();
                        //     var previewAspectRatio = data.width / data.height;

                        //     var previewImage = preview.getElementsByTagName('img').item(0);
                        //     var previewWidth = preview.offsetWidth;
                        //     var previewHeight = previewWidth / previewAspectRatio;
                        //     var imageScaledRatio = data.width / previewWidth;

                        //     preview.style.height = previewHeight + 'px';
                        //     previewImage.style.width = imageData.naturalWidth / imageScaledRatio + 'px';
                        //     previewImage.style.height = imageData.naturalHeight / imageScaledRatio + 'px';
                        //     previewImage.style.marginLeft = -data.x / imageScaledRatio + 'px';
                        //     previewImage.style.marginTop = -data.y / imageScaledRatio + 'px';
                        // }
                    });
                },
                mask_button_clicked: function(event) {
                    this.process_modal_task = "mask";
                    this.toggle_process_modal_buttons_to_default(false);
                    var image = document.getElementById('myProcessImage');
                    cropper = new myCropperJS(image, {
                        viewMode: 1,
                        guides: true, // show tic tac toe lines on cropper
                        center: true, // show fps + mark on cropper
                        highlight: true,
                        cropBoxMovable: true, // if true, when cursor within cropper cannot create new cropper by dragging, can move the existing cropper only
                        cropBoxResizable: true, // if false, after create cropper by dragging cannot resize
                        ready: function () {
                            croppable = true;
                            changeCSS("/css/myCropperJS/maskCropperArea.css");
                        }
                    });
                },
                confirm_button_clicked: function(event) {
                    if (!croppable) {
                        return;
                    }

                    // Setup
                    var setupCanvas = cropper.getCroppedCanvas();
                    var image = document.getElementById('myProcessImage');

                    if (this.process_modal_task == "mask") {
                        // Mask
                        var maskedCanvas = getMaskedCanvas(setupCanvas, image, cropper);

                        // Show
                        this.process_modal_final_src = maskedCanvas.toDataURL();
                    } else {
                        // Show
                        this.process_modal_final_src = setupCanvas.toDataURL();
                    }
                    this.cancel_button_clicked();
                },
                cancel_button_clicked: function(event) {
                    if (typeof cropper !== 'undefined') {
                        this.process_modal_task = "";
                        this.toggle_process_modal_buttons_to_default(true);
                        cropper.destroy();
                        croppable = false;
                    }
                },
                save_button_clicked: function(event) {
                    var image = document.getElementById('myFinalImage');
                    var canvas = document.createElement('canvas');
                    var context = canvas.getContext('2d');
                    var imageWidth = cropper.getImageData().naturalWidth;
                    var imageHeight = cropper.getImageData().naturalHeight;
                    var imageTop = cropper.getImageData().top;
                    var imageLeft = cropper.getImageData().left;
                    var imageAspect = cropper.getImageData().aspectRatio;

                    canvas.width = imageWidth;
                    canvas.height = imageHeight;

                    context.imageSmoothingEnabled = true;
                    context.drawImage(image, 0, 0, imageWidth, imageHeight);
                    context.fillRect(maskLeft, maskTop, maskWidth, maskHeight);
                    return canvas;

                    var initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    canvas.toBlob(function (blob) {
                        var formData = new FormData();

                        formData.append('avatar', blob, 'avatar.jpg');
                        $.ajax('https://jsonplaceholder.typicode.com/posts', {
                            method: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,

                            xhr: function () {
                                var xhr = new XMLHttpRequest();

                                xhr.upload.onprogress = function (e) {
                                    var percent = '0';
                                    var percentage = '0%';

                                    if (e.lengthComputable) {
                                        percent = Math.round((e.loaded / e.total) * 100);
                                        percentage = percent + '%';
                                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                    }
                                };

                                return xhr;
                            },
                            success: function () {
                                $alert.show().addClass('alert-success').text('Upload success');
                            },
                            complete: function () {
                                $progress.hide();
                            },
                        });
                    });
                },
                toggle_process_modal_buttons_to_default: function(defaultMode) {
                    if (!defaultMode) {
                        this.process_modal_crop_is_disabled = this.process_modal_mask_is_disabled = true;
                        this.process_modal_confirm_is_disabled = this.process_modal_cancel_is_disabled = false;
                    } else {
                        this.process_modal_crop_is_disabled = this.process_modal_mask_is_disabled = false;
                        this.process_modal_confirm_is_disabled = this.process_modal_cancel_is_disabled = true;
                    }
                },
                process_modal_dismissed: function(event) {
                    this.cancel_button_clicked();
                }
            },
            mounted: function() {
                $(this.$refs.process_modal).on("hidden.bs.modal", this.process_modal_dismissed)
            }
        });

        function changeCSS(myCropperCSSFile) {
            var oldlink = document.getElementById("myDynamicLink");

            var newlink = document.createElement("link");
            newlink.setAttribute("id", "myDynamicLink");
            newlink.setAttribute("rel", "stylesheet");
            newlink.setAttribute("type", "text/css");
            newlink.setAttribute("href", myCropperCSSFile);

            document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
        }

        function getMaskedCanvas(sourceCanvas, sourceImage, cropper) {
            var image = document.getElementById('myProcessImage');
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            var maskWidth = cropper.getData().width;
            var maskHeight = cropper.getData().height;
            var maskTop =  cropper.getData().y;
            var maskLeft =  cropper.getData().x;
            var imageWidth = cropper.getImageData().naturalWidth;
            var imageHeight = cropper.getImageData().naturalHeight;
            var imageTop = cropper.getImageData().top;
            var imageLeft = cropper.getImageData().left;
            var imageAspect = cropper.getImageData().aspectRatio;

            canvas.width = imageWidth;
            canvas.height = imageHeight;

            context.imageSmoothingEnabled = true;
            context.drawImage(image, 0, 0, imageWidth, imageHeight);
            context.fillRect(maskLeft, maskTop, maskWidth, maskHeight);
            return canvas;
        }
    });
</script>
@endsection
