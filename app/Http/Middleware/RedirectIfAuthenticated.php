<?php

namespace One\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            switch ($guard) {
                case 'admin':
                    return redirect()->route('client.profile.edit');
                    break;
                default:
                    return redirect()->route('dashboard');
                    break;
            }
        }

        return $next($request);
    }
}
