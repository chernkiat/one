<?php

namespace One\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                switch ($guard) {
                    case 'admin':
                        return redirect()->route('client.login.show');
                        break;
                    default:
                        return redirect()->route('auth.login.show');
                        break;
                }
            }
        }

        return $next($request);
    }
}
